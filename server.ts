import {
    Application, Context
} from "https://deno.land/x/oak@v6.3.2/mod.ts";
import { WebSocketMiddleware } from "https://raw.githubusercontent.com/jcc10/oak_websoket_middleware/v1.0.1/mod.ts";
import type { handler } from "https://raw.githubusercontent.com/jcc10/oak_websoket_middleware/v1.0.1/mod.ts";
import { WebSocket, isWebSocketCloseEvent, isWebSocketPingEvent } from 'https://deno.land/std@0.77.0/ws/mod.ts'
import { v4 } from "https://deno.land/std@0.77.0/uuid/mod.ts";

const queued = new Map<string, string>();

const socketHandler: handler = async function (socket) {
    // Wait for new messages
    try {
        for await (const ev of socket) {
            if(typeof ev === "string"){
                console.log("Got String: " + ev);
                socket.send(queued.get(ev) + "")
                queued.delete(ev);
            } else if (ev instanceof Uint8Array) {
                // binary message
                const view = new Uint8Array(ev);
                const blob = new Blob([view.buffer]);
                const text = await blob.text();
                const uuid = v4.generate();
                queued.set(uuid, text);
                console.log("Got blob: " + text);
                const outBlob = new Blob([uuid]);
                const outBuffer = await outBlob.arrayBuffer()
                const returning = new Uint8Array(outBuffer);
                socket.send(returning);
            } else if (isWebSocketPingEvent(ev)) {
                const [, body] = ev;
                // ping
                // stub
                //console.log("ws:Ping", body);
            } else if (isWebSocketCloseEvent(ev)) {
                // close
                const { code, reason } = ev;
                //console.log("ws:Close", code, reason);
            }
        }
    } catch (err) {
        console.error(`failed to receive frame: ${err}`);

        if (!socket.isClosed) {
            await socket.close(99).catch(console.error);
        }
    }
}

const app = new Application();
app.use(WebSocketMiddleware(socketHandler));
app.use(async (ctx: Context) => {
    const decoder = new TextDecoder("utf-8");
    const bytes = Deno.readFileSync("./client.html");
    const text = decoder.decode(bytes);
    ctx.response.body = text;
});
const appPromise = app.listen({ port: 3000 });
console.log("Server running on localhost:3000");